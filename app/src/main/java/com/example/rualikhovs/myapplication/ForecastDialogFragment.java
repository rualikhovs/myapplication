package com.example.rualikhovs.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by administrator on 01.06.17.
 */

public class ForecastDialogFragment extends DialogFragment {
    private static final String LOG_TAG = "PGDFragment";
    private ForecastAdapter forecastAdapter = null;
    private View main_view = null;
    private ArrayList<Symptom> symptoms;

    public enum Status {
        GOOD, WARN, BAD, UP, DOWN;
        private static final List<Status> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE_STATE = 3;
        private static final int SIZE_DIRECTION = 2;
        private static final Random RANDOM = new Random();

        public static Status randomStatus() {
            return VALUES.get(RANDOM.nextInt(SIZE_STATE));
        }

        public static Status getValue(int num) {
            return VALUES.get(num - 1);
        }

        public static Status getDirection(int num) {
            return VALUES.get(SIZE_STATE + num - 1);
        }


        public static Status randomDirection() {
            return VALUES.get(SIZE_STATE + RANDOM.nextInt(SIZE_DIRECTION));
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        inflater.inflate(R.layout.add_symptom_dialog, null);
        this.main_view = inflater.inflate(R.layout.forecast_dialog, null);
        this.forecastAdapter = new ForecastAdapter(getActivity(), getTestSymptoms());
        ListView lvForecastDialog = (ListView) main_view.findViewById(R.id.forecastDialogList);
        lvForecastDialog.setAdapter(this.forecastAdapter);
        builder.setView(main_view)
                // Add action buttons
                .setPositiveButton(R.string.string_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ForecastDialogFragment.this.getDialog().cancel();
                    }
                })
                .setTitle(R.string.string_current_forecast);
        return builder.create();
    }

    public void setBadSymptoms(ArrayList<Symptom> symptoms) {
        this.symptoms = symptoms;
    }

    private ArrayList<WarnLevel> getTestSymptoms() {
        final Date date = new Date();
        int hours = date.getHours();
        int[] values = null;
        int[] directions = null;

        if (hours >= 8 && hours < 20) {
            values = new int[]{1, 2, 3, 2, 1, 1, 2, 1, 3};
            directions = new int[]{1, 2, 2, 2, 1, 1, 2, 1, 2};
        } else {
            values = new int[]{2, 2, 1, 1, 1, 1, 3, 1, 3};
            directions = new int[]{2, 2, 1, 1, 2, 2, 1, 2, 1};
        }
        ArrayList<WarnLevel> res = new ArrayList<WarnLevel>();
        for (int i = 0; i < this.symptoms.size(); i++) {
            final String name = symptoms.get(i).name;
            res.add(new WarnLevel(name,
                    Status.getValue(values[i]),
                    Status.getDirection(directions[i])));
        }
        return res;
    }

    public class WarnLevel {
        public WarnLevel(String name, Status status, Status direction) {
            this.name = name;
            this.status = status;
            this.direction = direction;
        }

        public String name;
        public Status status;
        public Status direction;
    }
}
