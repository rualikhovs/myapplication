package com.example.rualikhovs.myapplication;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "MainActivity";
    private static final int REQUEST_PERMISSIONS_DELAY = 1000;
    private static final int DOUBLE_BACK_DELAY = 2000;

    private ArrayList<Symptom> symptomsList = new ArrayList<>();
    private SymptomAdapter boxAdapter;
    private WeatherObject weatherObject;
    private boolean doubleBackToExitPressedOnce = false;
    private AsyncInsert asyncInsert;
    private ForecastDialogFragment forecastDialogFragment;

    /**
     * Called when the activity is first created.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(getResources().getString(R.string.header_questions));
        requestPermissions();
        forecastDialogFragment = new ForecastDialogFragment();
    }

    @Override
    protected void onResume() {
        loadSymptoms();
        boxAdapter = new SymptomAdapter(this, symptomsList);
        ListView lvMain = (ListView) findViewById(R.id.listQuestions);
        lvMain.setAdapter(boxAdapter);
        lvMain.setEmptyView((RelativeLayout) findViewById(R.id.emptyView));
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        final String toast = getResources().getString(R.string.press_back_again);
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, DOUBLE_BACK_DELAY);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings: {
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_exit: {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            }

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }

    }

    public ArrayList<Symptom> getSymptoms() {
        return this.symptomsList;
    }

    /**
     * Загружает список доступных симтомов из базы данных при старте приложения
     */
    private void loadSymptoms() {
        final Uri uri = DataBaseContentProvider.SYMPTOMS_CONTENT_URI;
        final String projection[] = {DataBaseContentProvider.SYMPTOM_ID, DataBaseContentProvider.SYMPTOM_NAME, DataBaseContentProvider.SYMPTOM_DESCRIPTION, DataBaseContentProvider.SYMPTOM_TYPE, DataBaseContentProvider.SYMPTOM_DATE_UPDATED};
        final String selection = DataBaseContentProvider.SYMPTOM_USER_ID + " = ?";
        final String selectionArgs[] = {"1"};//default user
        final String sortOrder = DataBaseContentProvider.SYMPTOM_ID + " ASC";
        Cursor symptoms = getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder);
        int order = 0;
        symptomsList.clear();
        if (null == symptoms) {
            Log.d(LOG_TAG, "Cannot find table with URI " + uri.toString());
            return;
        }
        while (symptoms.moveToNext()) {
            long id = symptoms.getInt(symptoms.getColumnIndex(DataBaseContentProvider.SYMPTOM_ID));
            String name = symptoms.getString(symptoms.getColumnIndex(DataBaseContentProvider.SYMPTOM_NAME));
            String description = symptoms.getString(symptoms.getColumnIndex(DataBaseContentProvider.SYMPTOM_DESCRIPTION));
            int response = 146;//symptoms.getInt(symptoms.getColumnIndex(DataBaseContentProvider.SYMPTOM_));
            int type = symptoms.getInt(symptoms.getColumnIndex(DataBaseContentProvider.SYMPTOM_TYPE));
            if (type == Symptom.TYPE_RANGE)
                response = 1;
            this.symptomsList.add(order, new Symptom(id, name, description, response, type));
            order++;
        }
        symptoms.close();
        Log.d(LOG_TAG, "found " + symptoms.getCount() + " notes");
    }

    void requestPermissions() {
        final Activity thisActivity = this;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (ContextCompat.checkSelfPermission(thisActivity,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(thisActivity,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    } else {

                        ActivityCompat.requestPermissions(thisActivity,
                                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                1);
                    }
                }
            }
        }, REQUEST_PERMISSIONS_DELAY);
    }

    public void uploadData(View view) {
        asyncInsert = new AsyncInsert();
        asyncInsert.setSymptoms(symptomsList);
        asyncInsert.execute();
        Button button = (Button) view;
        button.setEnabled(false);
        button.setText(R.string.done);
    }

    public void showPrognose(View view) {

        forecastDialogFragment.setBadSymptoms(this.symptomsList);
        if(null != forecastDialogFragment)
            forecastDialogFragment.show(MainActivity.this.getFragmentManager(), "lol");
    }

    private class AsyncInsert extends AsyncTask<Void, Void, Void> {
        private ArrayList<Symptom> symptoms;

        void setSymptoms(ArrayList<Symptom> symptoms) {
            this.symptoms = symptoms;
        }

        @Override
        protected Void doInBackground(Void... params) {
            final long time = System.currentTimeMillis();
            final String projection[] = {DataBaseContentProvider.RESPONSES_GROUP, DataBaseContentProvider.RESPONSES_ID};
            final String sortOrder = DataBaseContentProvider.RESPONSES_GROUP + " DESC";
            Cursor cursor = MainActivity.this.getContentResolver().query(DataBaseContentProvider.RESPONSES_CONTENT_URI, projection, null, null, sortOrder);
            int group;
            //Получаем номер группы, если групп не было, то задаём его как 1
            if (null != cursor && 0 == cursor.getCount()) {
                group = 1;
            } else {
                cursor.moveToFirst();
                group = cursor.getInt(cursor.getColumnIndex(DataBaseContentProvider.RESPONSES_GROUP)) + 1;
            }
            cursor.close();
            for (int i = 0; i < symptoms.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(DataBaseContentProvider.RESPONSES_SYMPTOM_ID, symptoms.get(i).symptomId);
                cv.put(DataBaseContentProvider.RESPONSES_RESPONSE, symptoms.get(i).response);
                cv.put(DataBaseContentProvider.RESPONSES_GROUP, group);
                cv.put(DataBaseContentProvider.RESPONSES_DATE_UPDATED, time);
                Uri newUri = MainActivity.this.getContentResolver().insert(DataBaseContentProvider.RESPONSES_CONTENT_URI, cv);
                Log.d(LOG_TAG, newUri.toString());
            }
            return null;
        }
    }
}
