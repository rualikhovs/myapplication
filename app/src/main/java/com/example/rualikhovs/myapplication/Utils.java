package com.example.rualikhovs.myapplication;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by rualikhovs on 11.03.16.
 */
public class Utils {
    static JSONObject InputStreamToJSON(InputStream inputStream) throws IOException, JSONException {
        int ch;
        StringBuilder sb = new StringBuilder();
        while ((ch = inputStream.read()) != -1)
            sb.append((char) ch);
        return new JSONObject(sb.toString());
    }

    //TODO remove this
    static int getWrostSymptomValue(int amount) {
        final Date date = new Date();
        int hours = date.getHours();
        int[] values = null;
        //int[] directions = null;

        if (hours >= 8 && hours < 20) {
            values = new int[]{1, 1, 3, 2, 1, 1, 2, 1, 3};
            //directions = new int[]{1, 1, 2, 2, 1, 1, 2, 1, 2};
        } else {
            values = new int[]{2, 2, 1, 1, 1, 1, 3, 1, 3};
            //directions = new int[]{2, 2, 1, 1, 2, 2, 1, 2, 1};
        }

        int i, wrost = 1;
        for (i = 0; i < amount; i++) {
            if(values[i]>wrost){
                wrost = values[i];
            }
        }
        return wrost;
    }

    static void displayNotification(Context context, String title, String text) {
        if (null == context || null == title || null == text) {
            return;
        }
        //String ok = context.getResources(R.string.)
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_warning_black_24px)
                        .setContentTitle(title)
                        .setContentText(text);
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(001, mBuilder.build());
    }
}
