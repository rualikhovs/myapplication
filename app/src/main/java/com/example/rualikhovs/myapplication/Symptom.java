package com.example.rualikhovs.myapplication;

import android.content.ContentValues;
import android.util.Log;

/**
 * Created by rualikhovs on 19.09.16.
 */

public class Symptom {
    private static final String LOG_TAG = "Question";
    public static final int TYPE_RANGE = 0;
    public static final int TYPE_NUMBERIC = 1;
    //ContentValues cv;
    long symptomId;
    String name;
    String description;
    int type;//1 = numberic, 0 = radio check box
    int response;

    public Symptom(long id, String name, String description, int response, int type) {
        this.symptomId = id;
        this.name = name;
        this.description = description;
        this.setResponse(response);
        this.type = type;
        //cv = new ContentValues();
        //cv.put(DataBaseContentProvider.RESPONSES_SYMPTOM_ID, id);
        //cv.put(DataBaseContentProvider.RESPONSES_RESPONSE, response);
        //cv.put(DataBaseContentProvider.RESPONSES_SYMPTOM_ID, id);
    }

    public long getId() {
        return this.symptomId;
    }

    public String getName() {
        return name;
    }

    void setResponse(int response) {
        if (response < 1) {
            response = 1;
            Log.e(LOG_TAG, "bad response: " + response);
        }
        if (response > 5) {
            response = 5;
            Log.e(LOG_TAG, "bad response: " + response);
        }
        this.response = response;
    }
}
