package com.example.rualikhovs.myapplication;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by rualikhovs on 19.09.16.
 */

public class SymptomAdapter extends BaseAdapter {
    private static final String LOG_TAG = "SymptomAdapter";

    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<Symptom> objects;

    public SymptomAdapter(Context context, ArrayList<Symptom> symptoms) {
        ctx = context;
        objects = symptoms;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // symptom по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // symptomId по позиции
    @Override
    public long getItemId(int position) {
        return objects.get(position).getId();
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.question_list_item, parent, false);
        }

        Symptom response = getResponse(position);
        Log.d(LOG_TAG, "Type:" + response.type);

        EditText editText = (EditText) view.findViewById(R.id.numbericResponse);
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioQuestions);
        ((TextView) view.findViewById(R.id.note)).setText(response.name);
        if (Symptom.TYPE_RANGE == response.type) {
            radioGroup.setVisibility(View.VISIBLE);
            editText.setVisibility(View.GONE);
            radioGroup.setOnCheckedChangeListener(new OnSelectListener(position));
            switch (response.response) {
                case 1:
                    ((RadioButton) view.findViewById(R.id.radio0)).setChecked(true);
                    break;
                case 2:
                    ((RadioButton) view.findViewById(R.id.radio1)).setChecked(true);
                    break;
                case 3:
                    ((RadioButton) view.findViewById(R.id.radio2)).setChecked(true);
                    break;
                case 4:
                    ((RadioButton) view.findViewById(R.id.radio3)).setChecked(true);
                    break;
                case 5:
                    ((RadioButton) view.findViewById(R.id.radio4)).setChecked(true);
                    break;
                default:
            }
        } else {
            radioGroup.setVisibility(View.GONE);
            editText.setVisibility(View.VISIBLE);
            //editText.setText(response.response + "", TextView.BufferType.EDITABLE);
        }
        return view;
    }

    Symptom getResponse(int position) {
        return ((Symptom) getItem(position));
    }

    private class OnSelectListener implements RadioGroup.OnCheckedChangeListener {
        int position;

        OnSelectListener(int position) {
            this.position = position;
        }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            //SymptomAdapter.this.getItemId(position);
            switch (checkedId) {
                case R.id.radio0:
                    SymptomAdapter.this.objects.get(this.position).response = 1;
                    break;
                case R.id.radio1:
                    SymptomAdapter.this.objects.get(this.position).response = 2;
                    break;
                case R.id.radio2:
                    SymptomAdapter.this.objects.get(this.position).response = 3;
                    break;
                case R.id.radio3:
                    SymptomAdapter.this.objects.get(this.position).response = 4;
                    break;
                case R.id.radio4:
                    SymptomAdapter.this.objects.get(this.position).response = 5;
                    break;
                default:
                    throw new IllegalArgumentException("onCheckedChanged: Bad response");
            }
            Log.d(LOG_TAG, "Id " + SymptomAdapter.this.getItemId(position) + " set to:" + SymptomAdapter.this.objects.get(this.position).response);
        }
    }
}
