package com.example.rualikhovs.myapplication;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by rualikhovs on 28.01.16.
 */
public class WeatherService extends Service {

    private static final String LOG_TAG = "WeatherService";
    private static final int BOOT_DELAY = 1;//one minute
    private static final int SCHEDULE_DELAY = 1;//one hour

    private WeatherObject weatherObject;
    private Runnable dataRequestor;
    private boolean runned = false;

    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.i(LOG_TAG, "Service created");

        dataRequestor = new Runnable() {
            public void run() {
                Context context = getApplicationContext();
                ConnectivityManager cm =
                        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                boolean isConnected = false;
                if (null != activeNetwork)
                    isConnected = activeNetwork.isConnectedOrConnecting();

                if (isConnected) {
                    Log.i(LOG_TAG, "Requesting for the data...");
                    weatherObject = new WeatherObject(getApplicationContext());
                    //TODO uncomment this to get
                    //weatherObject.requestData();
                    //Write data to database
                    //JSONObject response = weatherObject.getServerResponse();
                } else {
                    Log.i(LOG_TAG, "No network available!");
                }
            }
        };
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "Service destroyed");
        runned = false;
        scheduler.shutdown();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "Service onStartCommand()");
        if (true != runned) {
            scheduler.scheduleAtFixedRate(dataRequestor, BOOT_DELAY, SCHEDULE_DELAY, MINUTES);
        }
        runned = true;
        return super.onStartCommand(intent, flags, startId);
    }

}
