package com.example.rualikhovs.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by rualikhovs on 02.06.17.
 */

public class ForecastAdapter extends BaseAdapter {
    private static final String LOG_TAG = "ForecastAdapter";

    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<ForecastDialogFragment.WarnLevel> symptoms;

    private long itemId = 0;

    public ForecastAdapter(Context context, ArrayList<ForecastDialogFragment.WarnLevel> symptoms) {
        this.ctx = context;
        this.symptoms = symptoms;
        this.lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return symptoms.size();
    }

    @Override
    public Object getItem(int position) {
        return this.symptoms.get(position);
    }

    @Override
    public long getItemId(int position) {
        itemId += 1;
        return itemId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.forecast_item, parent, false);
        }
        TextView textView = (TextView) view.findViewById(R.id.textViewItemName);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageViewItemWarning);
        ForecastDialogFragment.WarnLevel item = this.symptoms.get(position);
        textView.setText(item.name);
        if (item.direction == ForecastDialogFragment.Status.UP) {
            imageView.setImageDrawable(this.ctx.getResources().getDrawable(R.drawable.ic_trending_up_black_24px));
        } else {
            imageView.setImageDrawable(this.ctx.getResources().getDrawable(R.drawable.ic_trending_down_black_24px));
        }
        if (item.status == ForecastDialogFragment.Status.GOOD) {
            imageView.setColorFilter(this.ctx.getResources().getColor(R.color.colorGreen));
        } else if (item.status == ForecastDialogFragment.Status.WARN && ctx != null) {
            imageView.setColorFilter(this.ctx.getResources().getColor(R.color.colorOrange));
        } else if (item.status == ForecastDialogFragment.Status.BAD && ctx != null) {
            imageView.setColorFilter(this.ctx.getResources().getColor(R.color.colorRed));
        }
        return view;
    }
}
