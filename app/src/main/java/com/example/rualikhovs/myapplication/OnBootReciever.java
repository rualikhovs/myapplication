package com.example.rualikhovs.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by rualikhovs on 07.11.16.
 * Start weather service on boot with the device
 */

public class OnBootReciever extends BroadcastReceiver {
    private static final String LOG_TAG = "OnBootReciever";
    private static final long START_DELAY = 10*1000L;
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent newIntent = new Intent(context, WeatherService.class);
        PreferenceManager.setDefaultValues(context.getApplicationContext(), R.xml.pref_settings, false);
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE);
        boolean startup = sharedPreferences.getBoolean("startup", false);
        if (startup) {
            try {
                Thread.sleep(START_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ///context.startService(newIntent);
            Log.d(LOG_TAG, "Service was runned on boot");
        } else {
            Log.d(LOG_TAG, "Service was not runned on boot");
        }
    }
}
