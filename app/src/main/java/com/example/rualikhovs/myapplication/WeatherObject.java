package com.example.rualikhovs.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by rualikhovs on 22.01.16.
 */
public class WeatherObject {
    private static final String LOG_TAG = "WeatherObject";

    private static final String WEB_ADDRESS = "http://api.openweathermap.org/data/2.5/weather?";
    private static final String WEB_LATTITUDE = "lat=";
    private static final String WEB_LONGTITUDE = "lon=";
    private static final String WEB_ID = "APPID=";

    private Context appContext;
    private JSONObject serverResponse = null;
    private int serverResponseCode = 200;
    private double latitude;
    private double longtitude;
    private boolean isDataLoading = false;

    public JSONObject getServerResponse() {
        return serverResponse;
    }

    public WeatherObject(@NonNull Context context) {
        this.appContext = context;
        String serviceString = Context.LOCATION_SERVICE;
        LocationManager locationManager;
        locationManager = (LocationManager) this.appContext.getSystemService(serviceString);
        String provider = LocationManager.GPS_PROVIDER;
        if (ActivityCompat.checkSelfPermission(this.appContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.appContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast toast = Toast.makeText(context,
                    context.getResources().getString(R.string.toast_no_permission_gps), Toast.LENGTH_SHORT);
            toast.show();
        } else {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                this.latitude = location.getLatitude();
                this.longtitude = location.getLongitude();
            }
        }

    }

    public void requestData() {
        if (this.isDataLoading) return;
        final double lon = this.longtitude;
        final double lat = this.latitude;
        new Thread(new Runnable() {

            @Override
            public void run() {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    WeatherObject.this.isDataLoading = true;
                    String request = buildRequest(lon, lat);
                    url = new URL(request);
                    if (url == null) {
                        throw new NullPointerException("Server is not avilable");
                    }
                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    WeatherObject.this.serverResponse = Utils.InputStreamToJSON(inputStream);
                    WeatherObject.this.serverResponseCode = urlConnection.getResponseCode();
                } catch (MalformedURLException e) {
                    Log.e(LOG_TAG, buildRequest(lon, lat));
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Cannot resolve server");
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    isDataLoading = false;
                    if (urlConnection != null)
                        urlConnection.disconnect();
                }

            }
        }).start();
    }

    public String buildRequest(double longtitude, double lattitude) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(WEB_ADDRESS).
                append(WEB_LATTITUDE).append(lattitude).append('&').
                append(WEB_LONGTITUDE).append(longtitude).append('&').
                append(WEB_ID).append(appContext.getResources().getString(R.string.key_weather_api_key));
        Log.d(LOG_TAG, stringBuilder.toString());
        return stringBuilder.toString();
    }

    public void showData() {
        if (isDataLoading) {
            Log.d(LOG_TAG, "Loading is in progress");
            return;
        }
        if (serverResponse == null) {
            Log.d(LOG_TAG, "no data");
            return;
        }
        Log.d(LOG_TAG, serverResponse.toString());
    }
}
