package com.example.rualikhovs.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class SelfSetupDialogFragment extends DialogFragment {
    private static final String LOG_TAG = "SSDFragment";
    private View main_view = null;
    private Switch typeSwitch = null;
    private NumberPicker agePicker = null;
    private SharedPreferences sharedPreferences = null;
    private boolean gender = false;
    private int age = 30;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        inflater.inflate(R.layout.self_setup_dialog, null);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getApplicationContext());
        this.age = sharedPreferences.getInt("age", 30);
        this.gender = sharedPreferences.getBoolean("gender", false);
        this.main_view = inflater.inflate(R.layout.self_setup_dialog, null);
        this.agePicker = (NumberPicker) SelfSetupDialogFragment.this.main_view.findViewById(R.id.agePicker);
        this.agePicker.setMinValue(18);
        this.agePicker.setMaxValue(90);
        this.agePicker.setValue(age);
        this.typeSwitch = (Switch) SelfSetupDialogFragment.this.main_view.findViewById(R.id.switchGender);
        this.typeSwitch.setText(gender?getString(R.string.string_gender_female):getString(R.string.string_gender_male));
        this.typeSwitch.setChecked(gender);
        this.typeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SelfSetupDialogFragment.this.gender = isChecked;
                if (isChecked)
                    typeSwitch.setText(getString(R.string.string_gender_female), TextView.BufferType.NORMAL);
                else
                    typeSwitch.setText(getString(R.string.string_gender_male), TextView.BufferType.NORMAL);
            }
        });

        builder.setView(main_view)
                // Add action buttons
                .setPositiveButton(R.string.string_apply, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("age", agePicker.getValue());
                        editor.putBoolean("gender", SelfSetupDialogFragment.this.gender);
                        editor.commit();
                        Log.d(LOG_TAG, "onClick: preferences saved "+SelfSetupDialogFragment.this.gender+agePicker.getValue());
                    }
                })
                .setNegativeButton(R.string.string_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SelfSetupDialogFragment.this.getDialog().cancel();
                    }
                })
                .setTitle(R.string.pref_set_self_data)
                .setMessage(R.string.pref_set_self_data_summary);
        return builder.create();
    }
}
