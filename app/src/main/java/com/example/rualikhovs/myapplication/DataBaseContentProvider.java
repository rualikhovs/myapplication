package com.example.rualikhovs.myapplication;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;

public class DataBaseContentProvider extends ContentProvider {
    private static final String LOG_TAG = "DataBaseCP";
    public static final String AUTHORITY = "com.example.rualikhovs.myapplication.DataBaseContentProvider";

    public static final int DB_VERSION = 3;
    public static final String DB_NAME = "database";
    public static final String TABLE_WEATHER = "weather";
    public static final String TABLE_USERS = "user";
    public static final String TABLE_RESPONSES = "responses";
    public static final String TABLE_SYMPTOMS = "symptoms";

    public static final String WEATHER_ID = "_id";
    public static final String WEATHER_JSON = "weather_json";
    public static final String WEATHER_DATE = "date";
    public static final String WEATHER_DATE_UPDATED = "date_updated";

    public static final String USER_ID = "_id";
    public static final String USER_NAME = "name";
    public static final String USER_SNAME = "surname";
    public static final String USER_GENDER = "gender";
    public static final String USER_BIRTH_DATE = "birth_date";
    public static final String USER_WEIGHT = "weight";
    public static final String USER_DATE_UPDATED = "date_updated";

    public static final String RESPONSES_ID = "_id";
    public static final String RESPONSES_SYMPTOM_ID = "symptom_id";
    public static final String RESPONSES_GROUP = "group_id";
    public static final String RESPONSES_RESPONSE = "response";
    public static final String RESPONSES_DATE_UPDATED = "date_updated";

    public static final String SYMPTOM_ID = "_id";
    public static final String SYMPTOM_USER_ID = "user_id";
    public static final String SYMPTOM_NAME = "name";
    public static final String SYMPTOM_DESCRIPTION = "description";
    public static final String SYMPTOM_TYPE = "s_type";
    public static final String SYMPTOM_DATE_UPDATED = "date_updated";

    private static final String DB_WEATHER_CREATE = "create table " + TABLE_WEATHER + "(" +
            WEATHER_ID + " integer primary key autoincrement, " +
            WEATHER_JSON + " text, " +
            WEATHER_DATE + " text, " +
            WEATHER_DATE_UPDATED + " integer not null);";

    private static final String DB_USERS_CREATE = "create table " + TABLE_USERS + "(" +
            USER_ID + " integer primary key, " +
            USER_NAME + " text not null, " +
            USER_SNAME + " text, " +
            USER_GENDER + " integer, " +//0 = male; 1 == female
            USER_BIRTH_DATE + " integer, " +
            USER_WEIGHT + " integer, " +
            USER_DATE_UPDATED + " integer not null);";

    private static final String DB_RESPONSES_CREATE = "create table " + TABLE_RESPONSES + "(" +
            RESPONSES_ID + " integer primary key autoincrement, " +
            RESPONSES_SYMPTOM_ID + " integer not null, " +
            RESPONSES_GROUP + " integer not null, " +
            RESPONSES_RESPONSE + " integer not null, " +
            RESPONSES_DATE_UPDATED + " integer not null);";

    private static final String DB_SYMPTOMS_CREATE = "create table " + TABLE_SYMPTOMS + "(" +
            SYMPTOM_ID + " integer primary key autoincrement, " +
            SYMPTOM_USER_ID + " integer not null, " +
            SYMPTOM_NAME + " text not null, " +
            SYMPTOM_DESCRIPTION + " text, " +
            SYMPTOM_TYPE + " integer not null, " +
            SYMPTOM_DATE_UPDATED + " integer not null);";

    public static final Uri SYMPTOMS_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + TABLE_SYMPTOMS);
    public static final Uri RESPONSES_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + TABLE_RESPONSES);

    private static final int URI_USER = 1;
    private static final int URI_SYMPTOM = 4;
    private static final int URI_SYMPTOM_ID = 5;
    private static final int URI_RESPONSE = 6;
    private static final int URI_RESPONSE_ID = 7;
    //private static final int URI_DATE_FROM_TO = 4;
    //private static final int URI_DATE_UTC_FROM_TO = 5;

    DBHelper dbHelper = null;
    SQLiteDatabase db = null;

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, TABLE_USERS, URI_USER);
        uriMatcher.addURI(AUTHORITY, TABLE_SYMPTOMS, URI_SYMPTOM);
        uriMatcher.addURI(AUTHORITY, TABLE_SYMPTOMS + "/#", URI_SYMPTOM_ID);
        uriMatcher.addURI(AUTHORITY, TABLE_RESPONSES, URI_RESPONSE);
    }

    public DataBaseContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        int id = uriMatcher.match(uri);
        int count = 0;
        String table = null;
        if (URI_SYMPTOM == id) {
            table = TABLE_SYMPTOMS;
            db = dbHelper.getWritableDatabase();
            count = db.delete(table, null, null);
        } else if (URI_SYMPTOM_ID == id) {
            table = TABLE_SYMPTOMS;
            db = dbHelper.getWritableDatabase();
            String noteId[] = { uri.getLastPathSegment() };
            count = db.delete(table, "id=", noteId);
        } else if (URI_RESPONSE == id) {
            throw new UnsupportedOperationException("Not yet implemented");
        }
        // уведомляем ContentResolver, что данные по адресу resultUri изменились
        //if(resultUri != null)
        //    getContext().getContentResolver().notifyChange(resultUri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int id = uriMatcher.match(uri);
        String table = null;
        Uri resultUri = null;
        if (URI_SYMPTOM == id) {
            table = TABLE_SYMPTOMS;
            db = dbHelper.getWritableDatabase();
            long rowId = db.insert(table, null, values);
            resultUri = ContentUris.withAppendedId(SYMPTOMS_CONTENT_URI, rowId);
            Log.d(LOG_TAG, "Insert in "+ table +" done. Id: " + rowId);
        } else if (URI_RESPONSE == id) {
            table = TABLE_RESPONSES;
            db = dbHelper.getWritableDatabase();
            long rowId = db.insert(table, null, values);
            resultUri = ContentUris.withAppendedId(RESPONSES_CONTENT_URI, rowId);
            Log.d(LOG_TAG, "Insert in "+ table +" done. Id: " + rowId);
        }
        // уведомляем ContentResolver, что данные по адресу resultUri изменились
        if(resultUri != null)
            getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public boolean onCreate() {
        // TODO: Implement this to initialize your content provider on startup.
        dbHelper = new DBHelper(getContext());
        insertMockUser();
        Log.d(LOG_TAG, "Database created");
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.d(LOG_TAG, "query, " + uri.toString());
        int id = uriMatcher.match(uri);
        String table = null;
        String groupBy = null;
        String having = null;
        Uri notificationUri = null;
        if (URI_SYMPTOM == id) {
            table = TABLE_SYMPTOMS;
            notificationUri = SYMPTOMS_CONTENT_URI;
        } else if(URI_RESPONSE == id) {
            table = TABLE_RESPONSES;
            notificationUri = RESPONSES_CONTENT_URI;
        } else {
            return null;
        }
        Log.d(LOG_TAG, "Id: " + id + " Table: " + table);

        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(table, projection, selection, selectionArgs, groupBy, having, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), notificationUri);
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public Uri insertResponses(Uri uri, ArrayList<Symptom> values){
        final int id = uriMatcher.match(uri);
        long group = 1;//TODO
        final long date = System.currentTimeMillis();
        final String DELIMITER = ", ";
        //db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < values.size(); i++) {
            StringBuilder sb = new StringBuilder(256).append("INSERT INTO ")
                    .append(TABLE_RESPONSES + " (")
                    .append(RESPONSES_SYMPTOM_ID).append(DELIMITER)
                    .append(RESPONSES_GROUP).append(DELIMITER)
                    .append(RESPONSES_RESPONSE).append(DELIMITER)
                    .append(RESPONSES_DATE_UPDATED+") ")
                    .append(" VALUES (")
                    .append(values.get(i).symptomId).append(DELIMITER)
                    .append(group).append(DELIMITER)
                    .append(values.get(i).response).append(DELIMITER)
                    .append(date).append(");");
            String sqlCommand = sb.toString();
            Log.d(LOG_TAG, "SQL: " + sqlCommand);
            db.execSQL(sqlCommand);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return null;
    }

    /**
     * Add mock user if need
     */
    private void insertMockUser(){
        Log.d(LOG_TAG, "Try to add mock user");
        boolean needInsert = false;
        db = dbHelper.getWritableDatabase();
        String selection[] = {USER_ID};
        Cursor cursor = db.query(TABLE_USERS, selection, null,
                null, null, null, null);
        if (cursor.getCount() == 0) {
            needInsert = true;
            Log.d(LOG_TAG, "Mock user did not found");
        } else {
            cursor.moveToFirst();
            int id = cursor.getInt(cursor.getColumnIndex(USER_ID));
            Log.d(LOG_TAG, "Mock user did found: symptomId " + id);
        }
        cursor.close();

        if (needInsert) {
            ContentValues cv = new ContentValues();
            cv.put(USER_ID, 1);
            cv.put(USER_NAME, "user");
            cv.put(USER_SNAME, "default");
            cv.put(USER_GENDER, 0);
            cv.put(USER_WEIGHT, 70);
            cv.put(USER_BIRTH_DATE, 0);
            cv.put(USER_DATE_UPDATED, System.currentTimeMillis());
            db = dbHelper.getWritableDatabase();
            long rowID = db.insert(TABLE_USERS, null, cv);
            Log.d(LOG_TAG, "Row: symptomId " + rowID);
        }
    }

    private class DBHelper extends SQLiteOpenHelper {
        private static final String LOG_TAG = "DBHelper";

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("PRAGMA foreign_keys=ON;");
            db.execSQL(DB_USERS_CREATE);
            db.execSQL(DB_WEATHER_CREATE);
            db.execSQL(DB_RESPONSES_CREATE);
            db.execSQL(DB_SYMPTOMS_CREATE);
            Log.d(LOG_TAG, "Tables created");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
