package com.example.rualikhovs.myapplication;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by rualikhovs on 18.11.16.
 */

public class SettingsActivity extends PreferenceActivity {
    private static final String LOG_TAG = "SettingsActivity";
    private Context ctx = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.ctx = getApplicationContext();
        addPreferencesFromResource(R.xml.pref_settings);
        PreferenceManager.setDefaultValues(this, R.xml.pref_settings, false);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean notification = sharedPreferences.getBoolean("notification", true);
        boolean startup = sharedPreferences.getBoolean("startup", true);

        Preference button = (Preference) findPreference(getString(R.string.button_new_symptom));
        Preference buttonSetup = (Preference) findPreference(getString(R.string.button_set_self_data));
        Preference.OnPreferenceClickListener listener = new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (preference.getKey().equals(getString(R.string.button_new_symptom))) {
                    AddSymptomDialogFragment fragment = new AddSymptomDialogFragment();
                    fragment.show(SettingsActivity.this.getFragmentManager(), "lol");
                } else {
                    SelfSetupDialogFragment fragment = new SelfSetupDialogFragment();
                    fragment.show(SettingsActivity.this.getFragmentManager(), "lol");
                }
                return true;
            }
        };
        button.setOnPreferenceClickListener(listener);
        buttonSetup.setOnPreferenceClickListener(listener);

        Preference buttonClear = (Preference) findPreference(getString(R.string.button_clear_database));
        buttonClear.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                int delete = getContentResolver().delete(DataBaseContentProvider.SYMPTOMS_CONTENT_URI, null, null);
                Log.d(LOG_TAG, "Deleted " + delete + " rows");
                return true;
            }
        });

        Preference buttonStartSrv = (Preference) findPreference(getString(R.string.button_start_srv));
        buttonStartSrv.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startService(
                        new Intent(SettingsActivity.this, WeatherService.class));
                Log.d(LOG_TAG, "");
                return true;
            }
        });

        Preference buttonStopSrv = (Preference) findPreference(getString(R.string.button_stop_srv));
        buttonStopSrv.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                stopService(
                        new Intent(SettingsActivity.this, WeatherService.class));
                Log.d(LOG_TAG, "");
                return true;
            }
        });

        Preference buttonDispNotification = (Preference) findPreference(getString(R.string.button_display_notification));
        buttonDispNotification.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(SettingsActivity.this.ctx)
                                .setSmallIcon(R.drawable.ic_warning_black_24px)
                                .setContentTitle("My notification")
                                .setContentText("Hello World!");
                NotificationManager mNotifyMgr =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                // Builds the notification and issues it.
                mNotifyMgr.notify(001, mBuilder.build());
                return true;
            }
        });
    }
}
