package com.example.rualikhovs.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by administrator on 03.12.16.
 */

public class AddSymptomDialogFragment extends DialogFragment {

    private static final String LOG_TAG = "ASDFragment";
    private View main_view = null;
    private EditText name = null, description = null;
    private Switch typeSwitch = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        inflater.inflate(R.layout.add_symptom_dialog, null);
        this.main_view = inflater.inflate(R.layout.add_symptom_dialog, null);
        this.name = (EditText) AddSymptomDialogFragment.this.main_view.findViewById(R.id.editTextSymptomName);
        this.description = (EditText) AddSymptomDialogFragment.this.main_view.findViewById(R.id.editTextSymptomDscr);
        this.typeSwitch = (Switch) AddSymptomDialogFragment.this.main_view.findViewById(R.id.switchSymptomType);
        this.typeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                Log.d(LOG_TAG, "onCheckedChanged: " + isChecked);
                if (isChecked)
                    typeSwitch.setText(getString(R.string.string_numberic_type), TextView.BufferType.NORMAL);
                else
                    typeSwitch.setText(getString(R.string.string_range_type), TextView.BufferType.NORMAL);
            }
        });

        builder.setView(main_view)
                // Add action buttons
                .setPositiveButton(R.string.string_apply, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if (name.getText().toString().length() < 3) {
                            Log.d(LOG_TAG, "Text: " + name.getText().toString());
                            Toast.makeText(getActivity(), R.string.string_repeat_pls, Toast.LENGTH_SHORT).show();
                        } else {
                            final int type = typeSwitch.isChecked() ? Symptom.TYPE_NUMBERIC : Symptom.TYPE_RANGE;
                            ContentValues cv = generateInsert(name.getText().toString(), description.getText().toString(), type);
                            Uri newUri = getActivity().getContentResolver().insert(DataBaseContentProvider.SYMPTOMS_CONTENT_URI, cv);
                            Log.d(LOG_TAG, "Insert done. Uri: " + newUri.toString());
                        }
                    }
                })
                .setNegativeButton(R.string.string_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddSymptomDialogFragment.this.getDialog().cancel();
                    }
                })
                .setTitle(R.string.string_add_symptom)
                .setMessage(R.string.pref_new_symptom_summary);
        return builder.create();
    }

    private ContentValues generateInsert(String symptomName, String description, int type) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseContentProvider.SYMPTOM_USER_ID, 1);
        cv.put(DataBaseContentProvider.SYMPTOM_DATE_UPDATED, System.currentTimeMillis());
        cv.put(DataBaseContentProvider.SYMPTOM_NAME, symptomName);
        cv.put(DataBaseContentProvider.SYMPTOM_TYPE, type);
        if (description.length() != 0)
            cv.put(DataBaseContentProvider.SYMPTOM_DESCRIPTION, description);
        return cv;
    }
}
