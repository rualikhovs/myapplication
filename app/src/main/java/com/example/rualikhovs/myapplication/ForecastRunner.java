package com.example.rualikhovs.myapplication;

import android.util.Log;

import org.ejml.simple.SimpleMatrix;

/**
 * Created by administrator on 03.06.17.
 */

public class ForecastRunner {
    private static final String TAG = "ForecastRunner";

    private SimpleMatrix factors = null;

    private SimpleMatrix x = null;
    private SimpleMatrix xT = null;
    private SimpleMatrix y = null;

    ForecastRunner(double[][] factors, double[] parameter) {
        this.factors = new SimpleMatrix(factors);
        this.x = new SimpleMatrix(generateX(factors));
        this.y = new SimpleMatrix(generateY(parameter));
        this.xT = new SimpleMatrix(this.x.transpose());
    }

    public void voidCalcFotrecast() {
        if(null == x || null == y) {
            Log.e(TAG, "voidCalcFotrecast: no data", new NullPointerException());
            return;
        }
        SimpleMatrix xtx = this.xT.mult(this.x);
        SimpleMatrix xty = this.xT.mult(this.y);
        SimpleMatrix xtxInv = xtx.invert();
        SimpleMatrix b = xtxInv.mult(xty);
    }

    private double[][] generateX(double[][] factors) {
        final int height = factors.length;
        final int width = factors[0].length + 1;
        double[][] x = new double[height][width];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                if (0 != j) {
                    x[i][j] = factors[i][j - 1];
                } else {
                    x[i][j] = 1;
                }
            }
        }
        return x;
    }
    private double[][] generateY(double[] parameter) {
        double[][]y = new double[1][parameter.length];
        for (int i = 0; i < parameter.length; i++) {
            y[1][i] = parameter[i];
        }
        return y;
    }

    /*
    public static double[][] transponse(double[][] a) {
        final int height = a.length;
        final int width = a[0].length;
        double[][] t = new double[width][height];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                t[j][i] = a[i][j];
            }
        }
        return t;
    }

    public static double[][] multiplyMatrix(double[][] a, double[][] b) {
        double[][] result = new double[a.length][b[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                for (int k = 0; k < a[0].length; k++) {
                    result[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return result;
    }
    */
}
